# **Jadwal Faisal Pangestu**

<pre> Senin  09.00-16.00  | Jumat Kosong </pre>

<pre> Selasa 09.00-16.00  | Sabtu Kosong </pre>

<pre> Rabu   09.00-16.00  | Minggu Kosong </pre>

<pre> Kamis  09.00-16.00 </pre>

![screenshot-jadwal-dosen](https://koonek.id/assets/2022-10-01-19-13-28.png)

* Jadwal ini menyesuaikan jam kosong dosen pembimbing.

* Setiap hari, mahasiswa bimbingan diwajibkan datang ke kampus (kecuali hari Jumat karena jadwal dosen full).
